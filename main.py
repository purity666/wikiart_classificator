from tqdm import tqdm

import numpy as np
import pandas as pd

import torch
torch.set_printoptions(threshold=10_000)
from torch.utils.data import DataLoader, WeightedRandomSampler
from torchvision import models
import torch.nn as nn
from torch.nn import functional as F
import torch.optim as optim

import torchvision.transforms as tr

from dataset import WikiartDataset

def collate_fn(batch):
    batch = list(filter(lambda x: x is not None, batch))
    return torch.utils.data.dataloader.default_collate(batch)


def compute_metrics(confusion_matrix, class_hist=None):
    accuracy = np.trace(confusion_matrix) / np.sum(confusion_matrix)
    
    precision = []
    for class_id in range(confusion_matrix.shape[0]):
      precision.append(confusion_matrix[class_id, class_id] / np.sum(confusion_matrix[class_id, :]))

    recall = []
    for class_id in range(confusion_matrix.shape[0]):
      recall.append(confusion_matrix[class_id, class_id] / np.sum(confusion_matrix[:, class_id]))
    
    f1_score = []
    #f1_score_weighted = []
    for class_id in range(confusion_matrix.shape[0]):
      f1_score.append(2 * (precision[class_id]*recall[class_id]) / (precision[class_id]+recall[class_id]))
      #f1_score_weighted.append(f1_score[class_id]*class_hist[class_id]) / class_hist.shape[0]

    jaccard_score = []
    for class_id in range(confusion_matrix.shape[0]):
      jaccard_score.append(confusion_matrix[class_id, class_id] / (np.sum(confusion_matrix[class_id, :] + confusion_matrix[:, class_id]) - confusion_matrix[class_id, class_id]))

    #metrics = pd.DataFrame(list(zip(precision, recall, f1_score, f1_score_weighted)),
    metrics = pd.DataFrame(list(zip(precision, recall, f1_score, jaccard_score)),
                           columns=['precision', 'recall', 'f1', 'jaccard_score'])

    return metrics, np.mean(jaccard_score)


class Trainer():
    def __init__(self, settings: dict) -> None:
        self.settings = settings
        self.dataset_root = self.settings['dataset_root']
        batch_size = self.settings['batch_size']
        num_workers = self.settings['num_workers']
        self.grad_clip = settings['grad_clip']

        train_dataset = WikiartDataset(root=self.dataset_root,
            annotations_file=self.dataset_root+'style_train.csv',
            type='train',
            class_file=self.dataset_root+'style_class.txt')
        sampler = WeightedRandomSampler(train_dataset.sample_weights, len(train_dataset))
        self.train_dataloader = DataLoader(train_dataset,
                                           batch_size=batch_size,
                                           shuffle=False,
                                           num_workers=num_workers,
                                           collate_fn=collate_fn,
                                           pin_memory=True,
                                           sampler=sampler)

        val_dataset = WikiartDataset(root=self.dataset_root,
            annotations_file=self.dataset_root+'style_val.csv',
            type='val',
            class_file=self.dataset_root+'style_class.txt')
        self.val_dataloader = DataLoader(val_dataset,
                                         batch_size=batch_size,
                                         shuffle=False,
                                         num_workers=num_workers,
                                         collate_fn=collate_fn,
                                         pin_memory=True)

        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        print(self.device)

        self.train_transforms = torch.nn.Sequential(
            tr.RandomAffine(30, translate=(0.1, 0.3)),
            tr.RandomHorizontalFlip(),
            tr.RandomVerticalFlip(),
            tr.ColorJitter(brightness=.5, hue=.3)
        )

        self.model = models.resnet18(pretrained=True)

        for param in self.model.parameters():
            param.requires_grad = True
    
        num_ftrs = self.model.fc.in_features

        self.model.fc = nn.Sequential(
                    nn.Linear(num_ftrs, 512),
                    nn.ReLU(inplace=True),
                    nn.Linear(512, 512),
                    nn.Linear(512, 512),
                    nn.Linear(512, train_dataset.num_classes),
                    nn.LogSoftmax(dim=1))

        self.model = self.model.to(self.device)

        self.criterion_train = nn.CrossEntropyLoss(weight=train_dataset.class_weights).to(self.device)
        self.criterion_val = nn.CrossEntropyLoss(weight=val_dataset.class_weights).to(self.device)

        self.optimizer = optim.Adam(self.model.fc.parameters(),
                                   lr=settings['lr'])
        
        self.scheduler = optim.lr_scheduler.ReduceLROnPlateau(self.optimizer, 'min')

        self.train_loss = []
        self.val_loss = []

        self.confusion_matrix = np.zeros((train_dataset.num_classes,
                                          train_dataset.num_classes,), dtype=int)

    def train(self, epoch):
        self.confusion_matrix.fill(0)

        self.model.train()

        tbar = tqdm(self.train_dataloader, desc='train')
        train_loss = 0.0

        for i, sample in enumerate(tbar):
            if sample is None:
                print('Fuck!')
                continue
            image, gt = sample['image'].to(self.device), sample['label'].to(self.device)
            
            image = self.train_transforms(image)

            outputs = self.model(image)
            loss = self.criterion_train(outputs, gt)

            self.optimizer.zero_grad()
            loss.backward()

            if self.grad_clip:
                nn.utils.clip_grad_value_(self.model.parameters(), self.grad_clip)

            self.optimizer.step()

            _, preds = torch.max(outputs, 1)
            train_loss += loss.item()

            for p, t in zip(preds, gt):
                self.confusion_matrix[p, t] += 1

            tbar.set_description(f'Train loss: {train_loss/float(i+1):.4f}, Epoch: {epoch}')

        self.train_loss.append(train_loss / (i+1))
        

    def validate(self, epoch):
        self.confusion_matrix.fill(0)

        self.model.eval()

        tbar = tqdm(self.val_dataloader, desc='val')
        val_loss = 0.0

        for i, sample in enumerate(tbar):
            if sample is None:
                print('Fuck!')
                continue
            image, gt = sample['image'].to(self.device), sample['label'].to(self.device)

            with torch.no_grad():
                outputs = self.model(image)
            
            loss = self.criterion_val(outputs, gt)
            _, preds = torch.max(outputs, 1)
            val_loss += loss.item()

            for p, t in zip(preds, gt):
                self.confusion_matrix[p, t] += 1

            tbar.set_description(f'Val loss: {val_loss/float(i+1):.4f}, Epoch: {epoch}')

        self.val_loss.append(val_loss / (i+1))
        self.scheduler.step(self.val_loss[-1])

    def save_model(self, epoch):
        state = {
            'epoch': epoch,
            'state_dict': self.model.state_dict(),
            'optimizer': self.optimizer.state_dict(),

        }
        torch.save(state, f'checkpoint_{epoch}_epoch.pth')


if __name__ == '__main__':
    import argparse
    import wandb

    parser = argparse.ArgumentParser(description='Training image classifier on wikiart dataset.')
    parser.add_argument('--dataset_root', type=str, help='path to dataset root.', default='/content/wikiart/')
    args = parser.parse_args()

    wandb.init(project="wikiart_classifier_224")
    settings = {
            'dataset_root': args.dataset_root,
            'batch_size': 256,
            'num_workers': 16,
            'lr': 1e-4,
            'grad_clip': None,
            'epochs': 100,
            'weight_decay': 0.001
        }

    trainer = Trainer(settings)

    best_score = 0

    for epoch in range(settings['epochs']):
        log_dict = {}
        trainer.train(epoch)
        #print('train confusion matrics:')
        #print(trainer.confusion_matrix)
        metrics, jaccard_score = compute_metrics(trainer.confusion_matrix, )
        print('train metrics:')
        print(metrics, 'jaccard score=', jaccard_score)
        log_dict['train_loss'] = trainer.train_loss[-1]
        log_dict['train_score'] = jaccard_score
        log_dict['train_metrics'] = metrics

        trainer.validate(epoch)
        #print('val confusion matrics:')
        #print(trainer.confusion_matrix)
        metrics, jaccard_score = compute_metrics(trainer.confusion_matrix)
        print(metrics, 'jaccard score=', jaccard_score)
        log_dict['val_loss'] = trainer.val_loss[-1]
        log_dict['val_score'] = jaccard_score
        log_dict['val_metrics'] = metrics

        wandb.log(log_dict)

        print()

        if best_score < jaccard_score:
            best_score = jaccard_score

            trainer.save_model(epoch)
