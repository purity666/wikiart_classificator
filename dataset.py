from pathlib import Path

import numpy as np
import pandas as pd

import torch
from torch.utils.data import Dataset
from torchvision.io import read_image
from torchvision import transforms as tr


class WikiartDataset(Dataset):
    def __init__(self, root='./wikiart_224',
                annotations_file='./wikiart_224/style_train.csv',
                type='train',
                class_file='./wikiart_224/style_class.txt') -> None:
        self.root = root
        self.annotations_file = annotations_file
        self.class_file = class_file
        self.type = type

        self.annotations = pd.read_csv(self.annotations_file, header=None, names=['path', 'class'])
        
        self.class_decoder = {}
        with open(self.class_file, "r") as f:
            for line in f:
                (key, val) = line.split()
                self.class_decoder[int(key)] = val

        self.num_classes = len(self.class_decoder.keys())

        class_column = self.annotations['class']
        class_hist, _ = np.histogram(class_column, bins=range(self.num_classes+1))
        self.class_weights = 1. / torch.tensor(class_hist).float()
        self.sample_weights = self.class_weights[class_column]
        self.transform_p = [100. / sample_count for sample_count in class_hist]

        self.img_transforms = tr.Compose([tr.ConvertImageDtype(torch.float),
                                         tr.Normalize(mean=(0.5216, 0.4682, 0.4051),
                                                      std=(0.2107, 0.1995, 0.1880))])


    def __len__(self):
        return len(self.annotations.index)


    def __getitem__(self, index):
        data_row = self.annotations.iloc[[index]]
        img_path = Path(self.root, data_row['path'].item())
        gt_label = int(data_row['class'])

        try:
            img = read_image(str(img_path))
        except Exception as e:
            print(str(e))
            print('file:', img_path)
            print()
            return None
        
        img = self.img_transforms(img)

        return {'image': img,
                'label': gt_label}


if __name__ == '__main__':
    #dataset = WikiartDataset()
    root='./wikiart_224/'
    annotations_file=root+'style_val.csv'
    type='val'
    class_file=root+'style_class.txt'
    dataset = WikiartDataset(root, annotations_file, type, class_file)

    class_column = dataset.annotations['class']
    class_hist, class_bins = np.histogram(class_column, bins=range(dataset.num_classes+1))
    for i, item in enumerate(class_hist):
        print(dataset.class_decoder[i], ":", item)

    print()
    exit()

    NUM_SAMPLES = 700
    RANDOM_STATE = 1

    filtered_annotations = []

    for class_id in dataset.class_decoder.keys():
        print(dataset.class_decoder[class_id])
        class_id_df = dataset.annotations[dataset.annotations['class'] == class_id]
        if len(class_id_df) < NUM_SAMPLES:
            filtered_annotations.append(class_id_df)
            continue
        filtered_annotations.append(class_id_df.sample(n=NUM_SAMPLES, random_state=RANDOM_STATE))

    filtered_annotations = pd.concat(filtered_annotations)
    class_column = filtered_annotations['class']
    class_hist, class_bins = np.histogram(class_column, bins=range(dataset.num_classes+1))
    for i, item in enumerate(class_hist):
        print(dataset.class_decoder[i], ":", item)

    import shutil
    #cp from original wikiart to filtered
    from tqdm import tqdm
    for path in tqdm(filtered_annotations['path']):
        break
        origin_path = './data/wikiart' / Path(path)
        filtered_path = './data/reduced_wikiart' / Path(path)
        try:
            filtered_path.mkdir(parents=True, exist_ok=False)
        except FileExistsError:
            pass
        shutil.copy(origin_path, filtered_path)
    #save filtered csv
    #filtered_annotations.to_csv('./data/reduced_wikiart/style_train.csv', index=False)
    print('done')